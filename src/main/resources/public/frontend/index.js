import $ from 'jquery';
import styles from './styles.css';

window.$ = $;

const $brand = $('#brand');
const $category = $('#category');
const $measurement = $('#measurement');
const $measurementContainer = $('#measurement-container');
const $calculate = $('#calculate');
const $predictionContainer = $('#prediction-container');
const $prediction = $('#prediction');
const $ok = $('#ok');

function createElement(type, className, id, text) {
  return $('<' + type + '/>', {
    class: className,
    id: id,
    text: text
  });
}

function createOption(value, text) {
  return createElement('option', null, null, text)
    .val(value);
}

function getBrands() {
  return $.get('/brands');
}

function getCategories(brand) {
  return $.get('/categories?brand=' + brand);
}

function getPrediction(brand, category, measurement) {
  var url = '/prediction?brand=' + brand + '&category=' + category + '&measurement=' + measurement;
  return $.get(url);
}

function appendBrands(brands) {
  const defaultOption = createOption(null, 'Select a brand')
    .attr('selected', true);
  const options = [defaultOption];

  if (brands && brands.length) {
    for (let i = 0; i < brands.length; i++) {
      options.push(createOption(brands[i].key, brands[i].name));

    }
  }

  $brand
    .empty()
    .append(options);
}

function appendCategories(categories) {
  const defaultOption = createOption(null, 'Select a category')
    .attr('selected', true);
  const options = [defaultOption];

  if (categories && categories.length) {
    for (let i = 0; i < categories.length; i++) {
      options.push(createOption(categories[i].key, categories[i].name)
        .attr('data-measurement-type', categories[i].measurement_type));

    }
  }

  $category
    .empty()
    .append(options);
}

function updateMeasurementCaption(measurementType) {
  $measurementContainer.find('label').text(`My ${measurementType || ''} size is`);
}

function initialize() {
  // Append default values
  appendBrands();
  appendCategories();

  // Attach event handlers
  $brand.change(event => {
    appendCategories();
    updateMeasurementCaption();
    $category.attr('disabled', true);
    $measurementContainer.addClass('disabled');
    $measurement.attr('disabled', true).val('');
    $calculate.attr('disabled', true);


    const brand = $(event.delegateTarget).val();

    if (brand) {
      getCategories(brand).then(function({categories}) {
        appendCategories(categories);
        $category.attr('disabled', false);
      });
    }
  });

  $category.change(event => {
    $measurementContainer.addClass('disabled');
    $measurement.attr('disabled', true).val('');
    $calculate.attr('disabled', true);

    const category = $(event.delegateTarget).val();
    const measurementType = $(event.delegateTarget)
      .find('option:selected')
      .attr('data-measurement-type');

    updateMeasurementCaption(measurementType);

    if (category) {
      $measurementContainer.removeClass('disabled');
      $measurement.attr('disabled', false);
    }
  });

  $measurement.on('input', event => {
    const measurementValue = $(event.delegateTarget).val();

    if (measurementValue) {
      $calculate.attr('disabled', false);
    } else {
      $calculate.attr('disabled', true);
    }
  });

  $calculate.click(event => {
    const brand = $brand.val();
    const category = $category.val();
    const measurement = $measurement.val();

    getPrediction(brand, category, measurement).then(function({prediction: {labels}}) {
      $prediction.text(labels.join(' or '));
      $predictionContainer.removeClass('hidden');
    });
  });

  $ok.click(() => {
    $predictionContainer.addClass('hidden');
  });
}

initialize();

getBrands().then(function({brands}) {
  appendBrands(brands);
  $brand.attr('disabled', false);
});

