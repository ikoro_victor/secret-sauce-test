
$(document).ready(function(){
    var brandSelect = $('#brand');
    var categorySelect = $('#category')
    var measurementInput = $('#measurement')
    var inputContainer = $('#input-container')
    var calculateButton = $('#calculate')
    var okButton = $('#ok')
    var predictionLabel = $('#prediction')
    var errorLabel = $('#error')
    var predictionContainer = $('#prediction-container')

    //Load brands
    $.getJSON("/brands", {}, function(data){
        $.each( data.brands, function( index, val ) {
           $('#brand').append($('<option>', {
               value: val.key,
               text: val.name
           }));
          });
    })

    //Add change handler to brand select control, populate category on brand change
    brandSelect.on('change', function(){
        //Enable category select if disabled
        categorySelect.removeAttr( "disabled" )
        //Clear all options in category select
        categorySelect
            .find('option')
            .remove()
            .end()
            .append($('<option>', {
                value: "-1",
                text: "-- Select Category --"
            }));
        //Remove error text if any
        errorLabel.text(" ");

        //disable and clear measurement input text and calculate button
         measurementInput.attr( "disabled", true );
         measurementInput.val("");
         calculateButton.attr( "disabled", true );


        var brandVal = brandSelect.val();
        if(brand !== "-1"){
          $.getJSON("/categories", { brand:  brandVal}, function(data){
               $.each( data.categories, function( index, val ) {
                         categorySelect.append($('<option>', {
                             value: val.key,
                             text: val.name
                         }));
                    });
          })
          .fail(alertError);
        }
    })

    //Add change handler to  category select control
    categorySelect.on('change', function(){

        var categoryVal = categorySelect.val();
        if(categoryVal !== "-1"){
            //Enable measurement input, clear input
            measurementInput.removeAttr( "disabled" );
            measurementInput.val("");

        }
        else{
            //Disable measurement
            measurementInput.attr( "disabled", true );
            measurementInput.val("");
        }
    })


    //Add change handler for text Input
    measurementInput.on("keyup", function(){

        //Clear error text if there's any
        errorLabel.text(" ");

        var measurementVal = measurementInput.val().trim();

        if(measurementVal && parseFloat(measurementVal) !== NaN){
            //Enable calculate button
             calculateButton.removeAttr( "disabled" );
        }
        else{
            //Disable calculate button
             calculateButton.attr( "disabled", true );
        }
    })

    //Add Click handler for calculate button
    calculateButton.on('click', function(){
         //Disable the button till the prediction data is fully loaded
         $(this).attr("disabled", true);

         var measurementVal = measurementInput.val().trim();
         var categoryVal = categorySelect.val();
          var brandVal = brandSelect.val();

         //Fetch prediction
         $.getJSON("/prediction", {
                        measurement : measurementVal,
                        category : categoryVal,
                        brand : brandVal
                  },
             function(data){
                //Enable button, hide button and input container,  display prediction result or error
                calculateButton.removeAttr("disabled");
                predictionContainer.removeClass("hidden");
                var prediction = data.prediction.labels.join(", ");
                predictionLabel.text(prediction);
            })
         .fail(function(jqXHR, textStatus, errorThrown){
                //Enable calculate button,
                calculateButton.removeAttr("disabled");
                if(jqXHR.responseJSON) {
                    //Set error message on error label
                    errorLabel.text(jqXHR.responseJSON.error.message);
                }
                else{
                   alertError(jqXHR);
                }
          });
    });
    //Add OK click button handler
    okButton.on('click', function(){
        //Hide the prediction display
         predictionContainer.addClass("hidden");
    })

    var alertError = function(jqXHR){
         alert("Error: -" + jqXHR.status + " - " + jqXHR.responseText);
    }
})