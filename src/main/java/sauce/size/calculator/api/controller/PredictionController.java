package sauce.size.calculator.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sauce.size.calculator.exception.NotFoundException;
import sauce.size.calculator.model.Prediction;
import sauce.size.calculator.service.PredictionService;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
@RestController
@RequestMapping("/prediction")
public class PredictionController {

    private PredictionService predictionService;
    private MessageSource messageSource;

    @Autowired
    public PredictionController(PredictionService predictionService, MessageSource messageSource) {
        this.predictionService = predictionService;
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map prediction(@RequestParam String brand, @RequestParam String category, @RequestParam Double measurement){
        Prediction prediction = predictionService.getNearestMatch(brand, category, measurement);
        if(prediction == null)
        {
            throw new NotFoundException(
                    messageSource.getMessage("error.prediction.nomatch", new Object[]{Double.toString(measurement)}, Locale.ENGLISH));
        }
        return Collections.singletonMap("prediction", prediction );
    }
}
