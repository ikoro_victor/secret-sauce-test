package sauce.size.calculator.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sauce.size.calculator.service.BrandService;

import java.util.Collections;
import java.util.Map;

/**
 * Created by Victor Ikoro on 8/20/2017.
 */

@RestController
@RequestMapping("/brands")
public class BrandController {

    private BrandService brandService;

    @Autowired
    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map brands(){
        return Collections.singletonMap("brands", brandService.getBrands() );
    }
}
