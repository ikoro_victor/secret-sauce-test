package sauce.size.calculator.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sauce.size.calculator.service.CategoryService;

import java.util.Collections;
import java.util.Map;

/**
 * Created by Victor Ikoro on 8/20/2017.
 */

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Map categories(@RequestParam String brand){
        return Collections.singletonMap("categories", categoryService.getCategories(brand) );
    }
}
