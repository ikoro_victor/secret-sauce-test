package sauce.size.calculator.api.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;
import sauce.size.calculator.exception.InvalidParameterException;
import sauce.size.calculator.exception.NotFoundException;
import sauce.size.calculator.model.response.ErrorResponse;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
@ControllerAdvice(annotations = {RestController.class, Component.class})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiAdvice {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    private MessageSource messageSource;
    @Autowired
    public ApiAdvice(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(InvalidParameterException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map handleInvalidParameterException(InvalidParameterException e) {
        logger.debug(e.getMessage(), e);
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        return Collections.singletonMap("error", response);
    }

    @ExceptionHandler(ServletRequestBindingException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map handleServletRequestBindingException(ServletRequestBindingException e) {
        logger.debug(e.getMessage(), e);
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        return Collections.singletonMap("error", response);
    }



    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public Map handleNotFoundException(NotFoundException e) {
        logger.debug(e.getMessage(), e);
        ErrorResponse response = new ErrorResponse();
        response.setMessage(e.getMessage());
        return Collections.singletonMap("error", response);
    }
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map handleOtherExceptions(Exception e) {
        logger.error(e.getMessage(), e);
        ErrorResponse response = new ErrorResponse();
        response.setMessage(messageSource.getMessage(
                "error.processing.request",
                new Object[]{},
                Locale.ENGLISH
        ));
        return Collections.singletonMap("error", response);
    }
}
