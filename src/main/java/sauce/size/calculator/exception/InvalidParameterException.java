package sauce.size.calculator.exception;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
public class InvalidParameterException extends ApplicationException{
    public InvalidParameterException(String message) {
        super(message);
    }

    public InvalidParameterException(String message, Throwable cause) {
        super(message, cause);
    }
}
