package sauce.size.calculator.exception;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
public class NotFoundException extends ApplicationException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
