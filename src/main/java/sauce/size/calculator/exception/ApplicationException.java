package sauce.size.calculator.exception;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
public class ApplicationException extends RuntimeException {
    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
