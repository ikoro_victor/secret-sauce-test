package sauce.size.calculator.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import sauce.size.calculator.model.Brand;
import sauce.size.calculator.model.Category;
import sauce.size.calculator.model.Measurement;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Victor Ikoro on 8/20/2017.
 *
 * DAO for the mock json data store in classpath.
 * The json data in the file is similar the structure of a mongodb collection, with embedded relationships
 */
@Repository
@Qualifier("dataRepository")
public class JsonFileDataRepository implements DataRepository {

    private static final Logger logger = LoggerFactory.getLogger(JsonFileDataRepository.class);
    private List<Brand> brands;
    private Map<String , Brand> brandsMap;
    private Map<String , List<Category>> categoriesMap;
    private Map<String , Category> categoryMap;
    private Map<String , Measurement> measurementMap;
    private Map<String , List<Measurement>> measurementsMap;

    @Autowired
    public JsonFileDataRepository(@Value("${datastore.json.filename}") String jsonFilePath) {
        brands = new ArrayList<>();
        brandsMap = new HashMap<>();
        categoriesMap = new HashMap<>();
        categoryMap = new HashMap<>();
        measurementMap = new HashMap<>();
        measurementsMap = new HashMap<>();
        loadJSONFile(jsonFilePath);
    }

    @Override
    public Brand getBrand(String key) {
        return brandsMap.get(key);
    }

    @Override
    public List<Brand> getBrands() {
        return brands;
    }

    @Override
    public List<Category> getCategories(String brandKey) {
        return categoriesMap.get(brandKey);
    }

    @Override
    public Category getCategory(String brandKey, String categoryKey) {
        return categoryMap.get(buildKey(brandKey, categoryKey));
    }

    @Override
    public List<Measurement> getMeasurements(String brandKey) {
        return measurementsMap.get(brandKey);
    }

    @Override
    public Measurement getMeasurement(String brandKey, String measurementKey) {
        return measurementMap.get(buildKey(brandKey, measurementKey));
    }


    private void loadJSONFile( String jsonFilePath)
    {

        InputStream inputStream =
                    getClass().getClassLoader().getResourceAsStream(jsonFilePath);
        ObjectMapper mapper =  new ObjectMapper();
        try {
            List<BrandCollectionObject> objects  = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, BrandCollectionObject.class));
            for (BrandCollectionObject b :objects) {
                Brand brand = new Brand();
                brand.setKey(b.getKey());
                brand.setName(b.getName());
                brands.add(brand);
                brandsMap.put(brand.getKey(), brand);
                List<Category> categories =  b.getCategories();

                categoriesMap.put(brand.getKey(),categories);

                //Put each brand category in Map for constant time access;
                if(categories != null){
                    for (Category category: categories) {
                        String key = buildKey(brand.getKey(), category.getKey());
                        categoryMap.put(key, category );
                    }
                }

                List<Measurement> measurements = b.getMeasurements();
                measurementsMap.put(brand.getKey(),measurements);

                //Put each brand measurement in Map for constant time access;
                if( measurements != null){
                    for (Measurement measurement: measurements) {
                        String key = buildKey(brand.getKey(), measurement.getKey());
                        measurementMap.put(key, measurement);
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Could not load JSON file", e);
        }


    }
    private String buildKey(CharSequence... keys){
        return String.join("_", keys);
    }

    //Mock Brand object
    private static class BrandCollectionObject {
        private String key;
        private String name;
        private List<Category> categories;
        private List<Measurement> measurements;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Category> getCategories() {
            return categories;
        }

        public void setCategories(List<Category> categories) {
            this.categories = categories;
        }

        public List<Measurement> getMeasurements() {
            return measurements;
        }

        public void setMeasurements(List<Measurement> measurements) {
            this.measurements = measurements;
        }
    }
}
