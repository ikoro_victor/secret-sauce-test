package sauce.size.calculator.dao;

import sauce.size.calculator.model.Brand;
import sauce.size.calculator.model.Category;
import sauce.size.calculator.model.Measurement;

import java.util.List;

/**
 * Created by Victor Ikoro on 8/20/2017.
 */

public interface DataRepository {

    Brand getBrand(String key);
    List<Brand> getBrands();
    List<Category> getCategories(String brandKey);
    Category getCategory(String brandKey, String  categoryKey);
    List<Measurement> getMeasurements(String brandKey);
    Measurement getMeasurement(String brandKey, String measurementKey);

}
