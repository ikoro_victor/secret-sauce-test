package sauce.size.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sauce.size.calculator.dao.DataRepository;
import sauce.size.calculator.model.Category;

import java.util.List;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
@Service
public class CategoryService {

    private final DataRepository dataRepository;

    @Autowired
    public CategoryService(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public List<Category> getCategories(String brand){
         return dataRepository.getCategories(brand);
    }

}
