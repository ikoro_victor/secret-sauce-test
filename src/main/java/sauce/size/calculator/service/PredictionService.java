package sauce.size.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import sauce.size.calculator.dao.DataRepository;
import sauce.size.calculator.exception.InvalidParameterException;
import sauce.size.calculator.model.*;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
@Service
public class PredictionService {

    private final DataRepository dataRepository;
    private final MessageSource messageSource;
    private final double MEASUREMENT_MAX_VALUE;

    private final double INITIAL_MEASUREMENT_DISTANCE ;

    @Autowired
    public PredictionService(DataRepository dataRepository,
                             MessageSource messageSource,
                             @Value("${measurement.maximum:5000}") double measurement_max_value
    ) {
        this.dataRepository = dataRepository;
        this.messageSource = messageSource;
        MEASUREMENT_MAX_VALUE = measurement_max_value;
        INITIAL_MEASUREMENT_DISTANCE = measurement_max_value + 10000.0;
    }
    /*
      * The prediction logic works based on the following assumptions
      * 1. Step size for each measurement size on a size chart is fixed (e.g neck => (14-15.5), (16-17.5) - step size is 1.5)
      * 2. Size ranges in a size chart may or may not overlap(e.g. overlapping=> (25-30, 28-32), non-overlapping => (29-32, 33-35))
      * 3. If a measurement value falls between(or within) 2 size ranges, the closest size range is picked
      * 4. If there is no closest range (i.e. the distance between the ranges and the measurement value are the same) the larger size range is picked, so as to give room for adjustment
      * 5. Only one 'measurement_type' or 'dimension' is used to calculate the nearest match for each item category. Support for multi dimension will increase the time complexity.
      *    In a multi-dimension scenario, logarithmic approach will be needed for each dimension (as opposed to the current linear approach)
      * 6. A brand can support multiple size charts e.g Diane von Fusternberg supports a 0-14 and a P-L chart
     */
    public Prediction getNearestMatch(String brandKey, String categoryKey, double measurement) {
        if(measurement > MEASUREMENT_MAX_VALUE){
            throw new InvalidParameterException(
                    messageSource.getMessage("error.measurement.large", new Object[]{Double.toString(measurement), Double.toString(MEASUREMENT_MAX_VALUE)}, Locale.ENGLISH));
        }
        if(measurement < 0){
            throw new InvalidParameterException(
                    messageSource.getMessage("error.measurement.small", new Object[]{Double.toString(measurement)}, Locale.ENGLISH));
        }

        Brand brand = dataRepository.getBrand(brandKey);
        if (brand == null) {
            throw new InvalidParameterException(
                    messageSource.getMessage("error.brand.unknown", new Object[]{brandKey}, Locale.ENGLISH));
        }
        Category category = dataRepository.getCategory(brandKey, categoryKey);
        if (category == null) {
            throw new InvalidParameterException(
                    messageSource.getMessage("error.category.unknown", new Object[]{categoryKey, brandKey}, Locale.ENGLISH));
        }
        String measurementType = category.getMeasurementType();
        Measurement sizesMeasurement = dataRepository
                .getMeasurement(brandKey, measurementType);
        List<String> nearestMatches =  sizesMeasurement
                .getSizes()
                .entrySet()
                .stream()
                .map(entrySet -> {

                    List<Size> sizes = entrySet.getValue();

                    //Return null for empty or null size list
                    if (sizes == null || sizes.isEmpty()) return null;

                   //Perform a modified minimum distance search with the measurement
                    double minimumMinDistance = INITIAL_MEASUREMENT_DISTANCE;
                    double minimumMaxDistance = INITIAL_MEASUREMENT_DISTANCE;
                    int currentSizeMinMinIndex = -1;
                    int currentSizeMinMaxIndex = -1;
                    for(int i =  0; i < sizes.size(); i++){
                        Size size = sizes.get(i);
                        Size.Unit inches =  size.getInches();
                        double minSizeDistance = Math.abs(inches.getMin() - measurement);
                        double maxSizeDistance = Math.abs(inches.getMax() - measurement);
                        if (minSizeDistance == 0.0 && maxSizeDistance == 0.0){
                            //It's exact match, Return size
                            return size;
                        }
                        if(minSizeDistance < minimumMinDistance){
                            minimumMinDistance = minSizeDistance;
                            currentSizeMinMinIndex = i;

                        }

                        if(maxSizeDistance < minimumMaxDistance){
                            minimumMaxDistance = maxSizeDistance;
                            currentSizeMinMaxIndex = i;

                        }
                    }
                    Size minSize = sizes.get(currentSizeMinMinIndex); //The size which the 'min' is closest to the measurement value
                    Size maxSize = sizes.get(currentSizeMinMaxIndex); //The size which the 'max' is closest to the measurement value

                    /*
                    * Within size range. Don't return if measurement is equal to the 'max' of the minSize
                    * If the measurement equals the 'max' of the minSize, it might be 'between' and 'within' 2 range sizes ('max' of minSize & 'min' of maxSize)
                    */
                    if(measurement >= minSize.getInches().getMin() && measurement < minSize.getInches().getMax()){
                        return  minSize;
                    }

                    /*
                    * Within size range. Dont return if measurement is equal to the 'min' of the maxSize
                    * Might be within 2 ranges
                    */
                    if(measurement > maxSize.getInches().getMin() && measurement <= maxSize.getInches().getMax()){
                        return  maxSize;
                    }

                    /*
                    * Measurement is  definitely 'between' ranges and might or might not be 'within'
                    * i.e , 24 in between 21-23  and 25-27, or 30 in between and 'within' 25-30 and 30-35
                    * We find closest match
                    */
                    double minDiff = Math.abs(minSize.getInches().getMin() - measurement); //Difference between the 'min' of the size(which it's 'min' is closest to the measurement) and the measurement
                    double maxDiff = Math.abs(maxSize.getInches().getMax() - measurement); //Difference between the 'max' of the size (which it's 'max' is closest to the measurement) and the measurement

                    /*In this ternary operation, the maxSize is return only if the maxDiff is closer;
                     * e.g. Suppose minSize = (25-30) and maxSize = (19-24)
                     * if measurement is 24.3 therefore maxDiff will be  0.3 and minDiff 0.7, in this case we pick maxSize(19-24) (because its a closer range)
                     * if measurement is 24.7, maxDiff will be 0.7 and minDiff 0.3 then we pick minSize(25-30) beacuse its a closer range
                     * then if measurement is  24.5, maxDiff and minDiff (measurement is within 2 ranges) are the same, we pick minDiff since its a larger range(cloth adjustment rule)
                    */
                    return maxDiff < minDiff ? maxSize : minSize;


                })
                 //Filter for nulls and sizes above 'threshold'
                .filter(size -> {
                    if(size == null){
                        return false;
                    }
                    Size.Unit inches = size.getInches();

                    //Measurement within size range
                    if(measurement >= inches.getMin() &&  measurement <= inches.getMax()){
                        return  true;
                    }
                    double threshold =  sizesMeasurement.getSizeThreshold() != null ? Math.max(0, sizesMeasurement.getSizeThreshold().getInches()) : -1;

                    /*
                        Filter sizes which measurement is not within its range,
                        is flagged as the closest(because measurement was in between 2 ranges and it was the closest) but not within the size's Measurement threshold
                    */
                    if(threshold != -1){ //Threshold was set
                        if(Math.abs(inches.getMax() -  measurement) <= threshold ||
                                Math.abs(inches.getMin() - measurement) <= threshold ){
                            //Size within measurement threshold
                            return true;
                        }
                        else
                            return  false;
                    }
                    //No threshold was set, we assume the closest(irrespective of the distance gap) is fine
                    return true;

                })

                 .map(Size::getId)
                .collect(Collectors.toList());
        if (nearestMatches.size() > 0) {
            Prediction prediction = new Prediction();
            prediction.setLabels(nearestMatches);
            return prediction;
        }
        //No nearest match
        return null;
    }

}
