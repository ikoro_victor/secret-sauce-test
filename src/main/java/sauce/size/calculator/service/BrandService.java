package sauce.size.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sauce.size.calculator.dao.DataRepository;
import sauce.size.calculator.model.Brand;

import java.util.List;

/**
 * Created by Victor Ikoro on 8/20/2017.
 */
@Service
public class BrandService {

    private final DataRepository dataRepository;

    @Autowired
    public BrandService(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    public List<Brand> getBrands(){
        return dataRepository.getBrands();
    }
}
