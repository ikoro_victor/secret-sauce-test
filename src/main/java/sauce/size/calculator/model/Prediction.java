package sauce.size.calculator.model;

import java.util.List;

/**
 * Created by Victor.Ikoro on 8/21/2017.
 */
public class Prediction {

    private List<String> labels;

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }
}
