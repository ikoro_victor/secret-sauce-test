package sauce.size.calculator.model;

/**
 * Created by Victor Ikoro on 8/20/2017.
 */
public class Size {


    private String id;
    private Unit inches;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Unit getInches() {
        return inches;
    }

    public void setInches(Unit inches) {
        this.inches = inches;
    }

    public class Unit{
        private double min;
        private double max;

        public double getMin() {
            return min;
        }

        public void setMin(double min) {
            this.min = min;
        }

        public double getMax() {
            return max;
        }

        public void setMax(double max) {
            this.max = max;
        }
    }
}
