package sauce.size.calculator.model;

/**
 * Created by Victor.Ikoro on 8/24/2017.
 */
public class SizeThreshold {

    private double inches;

    public double getInches() {
        return inches;
    }

    public void setInches(double inches) {
        this.inches = inches;
    }
}
