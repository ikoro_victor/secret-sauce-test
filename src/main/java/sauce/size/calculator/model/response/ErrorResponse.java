package sauce.size.calculator.model.response;

/**
 * Created by Victor.Ikoro on 8/23/2017.
 */
public class ErrorResponse {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
