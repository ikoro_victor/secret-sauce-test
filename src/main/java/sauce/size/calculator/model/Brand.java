package sauce.size.calculator.model;

/**
 * Created by Victor Ikoro on 8/20/2017.
 */
public class Brand {
    private String key;
    private String name;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
