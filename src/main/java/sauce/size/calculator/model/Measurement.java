package sauce.size.calculator.model;

import java.util.List;
import java.util.Map;

/**
 * Created by Victor Ikoro on 8/20/2017.
 *
 */
public class Measurement {
    private String key;
    private Map<String, List<Size>> sizes;
    private SizeThreshold sizeThreshold;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, List<Size>> getSizes() {
        return sizes;
    }

    public void setSizes(Map<String, List<Size>> sizes) {
        this.sizes = sizes;
    }

    public SizeThreshold getSizeThreshold() {
        return sizeThreshold;
    }

    public void setSizeThreshold(SizeThreshold sizeThreshold) {
        this.sizeThreshold = sizeThreshold;
    }
}
