import org.junit.Before;
import org.junit.Test;
import org.springframework.context.MessageSource;
import sauce.size.calculator.dao.JsonFileDataRepository;
import sauce.size.calculator.exception.InvalidParameterException;
import sauce.size.calculator.model.Prediction;
import sauce.size.calculator.service.PredictionService;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Victor Ikoro on 8/21/2017.
 */
public class PredictionTest {

    private PredictionService predictionService;
    private final double MAX_MEASUREMENT = 10000;
    @Before
    public void setup(){
        MessageSource messageSource = mock(MessageSource.class);
        when(messageSource.getMessage(anyString(), any(),any())).thenReturn("testMessage");
        predictionService =  new PredictionService( new JsonFileDataRepository("test_data_store.json"), messageSource, MAX_MEASUREMENT);
    }


    @Test
    public void testPredictionClosestMatchMeasurementAllWithinRange(){
        Prediction prediction =  predictionService.getNearestMatch("test-brand-1", "dresses", 28 );
        assertEquals("Expected 3 matches", prediction.getLabels().size(), 3);
        assertTrue("Expected 'OVL-Size-7' from OverLappingStepChart", prediction.getLabels().contains("OVL-Size-7"));
        assertTrue("Expected 'RNGEX-E-28' from RangeAndExactSizeChart" ,prediction.getLabels().contains("RNGEX-E-28"));
        assertTrue("Expected 'EX-AC' from ExactSizeChart", prediction.getLabels().contains("EX-AC"));

    }

    //test closest match (with threshold factor)
    @Test
    public void testPredictionClosestMatch3of3ChartsWithThreshold(){
        Prediction prediction =  predictionService.getNearestMatch("test-brand-1", "dresses", 29 );
        assertEquals("Expected 3 matches", prediction.getLabels().size(), 3);
        assertTrue("Expected 'OVL-Size-7' from OverLappingStepChart", prediction.getLabels().contains("OVL-Size-7"));

    }



    /* 11 inches glove exist both as an exact size and range size '10-12' in 'RangeAndExactSizeChart' for test-brand-1
     * Pick the exact size over the range size
     */
    @Test
    public void testPredictionClosestMatchExactOverRangeSize(){
        Prediction prediction =  predictionService.getNearestMatch("test-brand-1", "gloves", 11 );
        assertEquals("Expected 3 matches, 'OVL-Size-2.5' ,  'RNGEX-E-11' & EX-C(closest and within threshold)", prediction.getLabels().size(), 3);
        assertTrue("Expected 'RNGEX-E-11' from RangeAndExactSizeChart", prediction.getLabels().contains("RNGEX-E-11"));
        assertFalse("Unexpected  size 'RNGEX-R-10-12' from RangeAndExactSizeChart", prediction.getLabels().contains("RNGEX-R-10-12"));

    }

    /*
    *   39.1 inches shirt matches falls in between 2 range sizes S(36.5- 38.5) and M(39.5- 41.5) in 'S-XL' for black-diamond-men
    *   Pick the size range which the measurement (39.1) is closer to (which is M, since the measurement is closer to the min of M than the max of S)
    *   and is within threshold
    */
    @Test
    public void testPredictionInBetweenRanges(){

        Prediction prediction =  predictionService.getNearestMatch("black-diamond-men", "shirts", 39.1 );
        assertNotNull("Expected a prediction result", prediction);
        assertTrue("Expected  'M' from S-XL ", prediction.getLabels().contains("M"));
        assertFalse("Unexpected  Size 'S' from S-XL ", prediction.getLabels().contains("S"));
    }
    /*
     * This is the reverse of the test case 'testPredictionInBetweenRanges'.
     * Here we pick S(since the measurement(38.9) is closer to the max of S than the min of M and is within threshold)
     *
     */
    @Test
    public void testPredictionInBetweenRangesReverse(){

        Prediction prediction =  predictionService.getNearestMatch("black-diamond-men", "shirts", 38.9 );
        assertNotNull("Expected a prediction result", prediction);
        assertTrue("Expected  'S' from S-XL ", prediction.getLabels().contains("S"));
        assertFalse("Unexpected  Size 'M' from S-XL ", prediction.getLabels().contains("M"));
    }

    /*
  *   33.9 inches shirt matches falls in between 2 range sizes 82(32.3- 33.9) and 84(33.9-41.5) in 'EN_13402_MEN' for diane-von-fusternberg
  *   Note that the measurement (33.9) falls 'between'  the 2 ranges and also 'within' both ranges unlike 'S-XL' in black-diamond-men  where it only falls 'between'
  *   This is common for charts that use the EN_13402 label standard.
  *   Since the measurement falls 'within' both ranges, we pick the size range which the measurement (39.1) is is farther from the max of the range
  *   This is to leave room for cloth adjustment
  *
  */
    @Test
    public void testPredictionInBetweenAndWithinRanges(){

        Prediction prediction =  predictionService.getNearestMatch("diane-von-furstenberg", "shirts", 33.9 );
        assertNotNull("Expected a prediction result", prediction);
        assertEquals("Expected 3 matches, P (closer and within threshold), 12 (within threshold too), 84 ", prediction.getLabels().size(), 3);
        assertTrue("Expected  '84' from EN_13402_MEN ", prediction.getLabels().contains("84"));
        assertFalse("Unexpected  Size '82' from EN_13402_MEN ", prediction.getLabels().contains("82"));
    }



    /*
    *   30 inches dress matches OVL-Size-6.5 ('26-31') and OVL-Size-7('28-33') in 'OverLappingStepChart' for test-brand-1
    *   We pick the size where the measurement (30) is the farther from the range max for each size (31,33),
    *   which is OVL-Size-7(33). This is to leave room for cloth adjustment
   */
    @Test
    public void testPredictionClosestMatchFarthestMaxSize(){
        Prediction prediction =  predictionService.getNearestMatch("test-brand-1", "dresses", 30 );
        assertEquals("Expected 2 matches, 'OVL-Size-7' 'RNGEX-E-30'", prediction.getLabels().size(), 2);
        assertTrue("Expected 'OVL-Size-7' from OverLappingStepChart", prediction.getLabels().contains("OVL-Size-7"));
        assertFalse("Unexpected  size 'OVL-Size-6.5' from OverLappingStepChart", prediction.getLabels().contains("OVL-Size-6.5"));
    }

    @Test
    public void testPredictionNoMatch(){
        Prediction prediction =  predictionService.getNearestMatch("test-brand-1", "dresses", 5000 );
        assertNull("Expected 0 matches", prediction);
    }


    @Test
    public void testPredictionDianeVonFurstenberg(){
        Prediction prediction =  predictionService.getNearestMatch("diane-von-furstenberg", "jeans", 43 );
        assertEquals(prediction.getLabels().size(), 2);
        assertTrue(prediction.getLabels().contains("14"));
        assertTrue(prediction.getLabels().contains("L"));

    }

    @Test
    public void testPredictionDianeVonFurstenberg2(){
        Prediction prediction =  predictionService.getNearestMatch("diane-von-furstenberg", "dresses", 36.5 );
        assertEquals("Expected 3 matches, 8, 86 , M ",prediction.getLabels().size(), 3);
        assertTrue(prediction.getLabels().contains("8"));
        assertTrue(prediction.getLabels().contains("M"));
        assertTrue(prediction.getLabels().contains("86"));

    }
    @Test
    public void testPredictionDianeVonFurstenbergWithClosestMatchRange(){
        Prediction prediction =  predictionService.getNearestMatch("diane-von-furstenberg", "dresses", 37.1 );
        assertEquals("Expected 3 matches, 10 (closer and within threshold), 86 , M ",prediction.getLabels().size(), 3);
        assertTrue(prediction.getLabels().contains("M"));
        assertTrue(prediction.getLabels().contains("86"));
        assertTrue(prediction.getLabels().contains("10"));

    }

    @Test(expected = InvalidParameterException.class)
    public void testPredictionInvalidBrand(){
         predictionService.getNearestMatch("diane-von-invalid", "dresses", 37.1 );
    }

    @Test(expected = InvalidParameterException.class)
    public void testPredictionInvalidCategory(){
        predictionService.getNearestMatch("diane-von-furstenberg", "dresses-invalid", 37.1 );
    }

    @Test(expected = InvalidParameterException.class)
    public void testPredictionInvalidMeasurementTooLarge(){
        predictionService.getNearestMatch("diane-von-furstenberg", "dresses", MAX_MEASUREMENT + 1 );
    }
    @Test(expected = InvalidParameterException.class)
    public void testPredictionInvalidMeasurementTooSmall(){
        predictionService.getNearestMatch("diane-von-furstenberg", "dresses", -1 );
    }

    /*
       * Largest 'S-XL' size for 'neck' measurement in 'diamond-black-men' is '17.5' inches ,
       * and since no threshold was set, the measurement value '50' for 'bow-tie' should return 'XL'
     */

    @Test
    public void testPredictionNoThresholdSetSizesUpperBound(){
        Prediction prediction = predictionService.getNearestMatch("black-diamond-men", "bow-tie", 50 );
        assertNotNull("Expected a prediction result", prediction);
        assertTrue("Expected XL from 'S-XL' chart", prediction.getLabels().contains("XL"));

    }

    /*
       * Smallest 'S-XL' size for 'neck' measurement in 'diamond-black-men' is '14.5' inches ,
       * and since no threshold was set, the measurement value '6' for 'bow-tie' should return 'S'
     */
    @Test
    public void testPredictionNoThresholdSetSizesLowerBound(){
        Prediction prediction = predictionService.getNearestMatch("black-diamond-men", "bow-tie", 6 );
        assertNotNull("Expected a prediction result", prediction);
        assertTrue("Expected S from 'S-XL' chart", prediction.getLabels().contains("S"));

    }

      /*
       * Largest 'S-XL' size for 'chest' measurement in 'diamond-black-men' is '47.5' inches ,
       * and since threshold was set(+-0.5), the measurement value '55' for 'shirts' should not return
       * a prediction while '48' should return 'XL'
     */

    @Test
    public void testPredictionThresholdSetSizesUpperBound(){
        Prediction noPrediction = predictionService.getNearestMatch("black-diamond-men", "shirts", 55 );
        Prediction prediction = predictionService.getNearestMatch("black-diamond-men", "shirts", 48 );
        assertNull("No prediction result expected for measurement value '55'", noPrediction);
        assertNotNull("Expected a prediction result measurement value '48'", prediction);
        assertTrue("Expected XL from 'S-XL' chart", prediction.getLabels().contains("XL"));

    }

    /*
       * Smallest 'S-XL' size for 'chest' measurement in 'diamond-black-men' is '36.5' inches ,
       * and since threshold was set(+-0.5), the measurement value '36' for 'shirts' should return 'S'
       * and '28' should not return a prediction
     */
    @Test
    public void testPredictionThresholdSetSizesLowerBound(){
        Prediction noPrediction = predictionService.getNearestMatch("black-diamond-men", "shirts", 28 );
        Prediction prediction = predictionService.getNearestMatch("black-diamond-men", "shirts", 36 );
        assertNull("No prediction result expected for measurement value '28'", noPrediction);
        assertNotNull("Expected a prediction result measurement value '36'", prediction);
        assertTrue("Expected S from 'S-XL' chart", prediction.getLabels().contains("S"));

    }



}
