import org.junit.Before;
import org.junit.Test;
import sauce.size.calculator.dao.JsonFileDataRepository;
import sauce.size.calculator.model.Category;
import sauce.size.calculator.service.CategoryService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Victor.Ikoro on 8/23/2017.
 */

public class CategoryTest {

    private CategoryService categoryService;
    @Before
    public void setup(){
        categoryService =  new CategoryService( new JsonFileDataRepository("test_data_store.json"));
    }

    @Test
    public void testCategoryCount(){
        List<Category> categories = categoryService.getCategories("test-brand-1");
        assertNotNull("Expects categories != null", categories);
        assertEquals("Expects 2 categories", categories.size(), 2);
    }

    @Test
    public void testNoCategoriesForUnknownBrand(){
        List<Category> categories = categoryService.getCategories("unknown-brand-1");
        assertNull("Expects categories to be null", categories);
    }



}
