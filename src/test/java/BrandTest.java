import org.junit.Before;
import org.junit.Test;
import sauce.size.calculator.dao.JsonFileDataRepository;
import sauce.size.calculator.model.Brand;
import sauce.size.calculator.service.BrandService;

import java.util.List;


import static org.junit.Assert.*;

/**
 * Created by Victor.Ikoro on 8/23/2017.
 */
public class BrandTest {

    private BrandService brandService;
    @Before
    public void setup(){
        brandService =  new BrandService( new JsonFileDataRepository("test_data_store.json"));
    }

    @Test
    public void testBrandCount(){
        List<Brand> brands = brandService.getBrands();
        assertEquals("Expects 3 brands", brands.size(), 3);
    }




}
