import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import sauce.size.calculator.Application;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Victor.Ikoro on 8/23/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class ControllersTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnBrands() throws Exception {
        this.mockMvc.perform(get("/brands")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("brands")))
                .andExpect(content().string(containsString("diane-von-furstenberg")));
    }

    @Test
    public void shouldReturnCategories() throws Exception {
        this.mockMvc.perform(get("/categories?brand=diane-von-furstenberg")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("categories")))
                .andExpect(content().string(containsString("dresses")));
    }

    @Test
    public void shouldReturnPredictions() throws Exception {
        this.mockMvc.perform(get("/prediction?brand=diane-von-furstenberg&category=dresses&measurement=33.5")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("prediction")))
                .andExpect(content().string(containsString("labels")))
                .andExpect(content().string(containsString("P")))
                .andExpect(content().string(containsString("2")));
    }
}
